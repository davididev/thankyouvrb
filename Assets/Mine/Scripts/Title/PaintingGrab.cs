﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PaintingGrab : MonoBehaviour
{
    public string sceneToLoad = "ThanksEric";


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnGrip(bool isLeft)
    {
        DialogueHandler.GetInstance().EndEvent();
        PlayerMovement.TitlePos = PlayerMovement.PlayerPos;
        //PlayerMovement.TeleportPos = Vector3.zero;

        DialogueChangeScene[] scenes = new DialogueChangeScene[1];
        scenes[0] = new DialogueChangeScene();
        scenes[0].breakpoint = true;
        scenes[0].sceneToMoveTo = sceneToLoad;
        scenes[0].positionToMoveTo = Vector3.zero;

        DialogueHandler.GetInstance().StartEvent(scenes);

        //SceneManager.LoadScene(sceneToLoad);
        gameObject.SetActive(false);
    }
}
