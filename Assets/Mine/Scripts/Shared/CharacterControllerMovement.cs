﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterControllerMovement : MonoBehaviour
{
    public CharacterController cc;
    public float maxSpeed = 3f, accelerationPerSecond = 6f;
    private const float TERMINAL_VELOCITY = 40f;
    private float forwardSpeed = 0f, rightSpeed = 0f;
    public Vector2 moveVec = Vector2.zero;
    private float gravity = 0f;

    public float waterVeritcal = 0f;

    public static float gravityScale = 1f;

    public Vector3 bottomPos {private set; get;}  //The foot position of the character controller
    public Vector3 topPos { private set; get; } //The head position of the character controller

    private Force[] forces;

    private class Force
    {
        public Vector3 force = Vector3.zero;
        public float maxTime = 1f;
        private float currentTime = 0f;

        public Force()
        {

        }

        /// <summary>
        /// Add a force
        /// </summary>
        /// <param name="frc">Direction and strength of the force</param>
        /// <param name="time">How long should this push last?  Higher times may cause you to go further</param>
        public void AddForce(Vector3 frc, float time)
        {
            this.force = frc;
            this.maxTime = time;
            this.currentTime = time;
        }

        public void Clear()
        {
            currentTime = 0f;
        }

        /// <summary>
        /// Should be called on update to go along with the other Character Controller movement.
        /// </summary>
        /// <returns></returns>
        public Vector3 CurrentVelocity()
        {
            if (currentTime <= 0f)
                return Vector3.zero;

            float perc = this.currentTime / this.maxTime;
            currentTime -= Time.deltaTime;
            Vector3 rel = this.force * perc * Time.deltaTime;
            //Debug.Log("Force: " + this.force + "Time: " + perc);
            return rel;
        }

        public bool IsEmpty()
        {
            return (currentTime <= 0f);
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        forces = new Force[50];
        for(int i = 0; i < forces.Length; i++)
        {
            forces[i] = new Force();
        }
    }

    /// <summary>
    /// This is similar to Rigid body's force, but works under the hood very differently.
    /// </summary>
    /// <param name="v">The direction and magnitude of the force</param>
    /// <param name="t">How long the force should last per second.  Higher times may cause you to go further</param>
    public void AddForce(Vector3 v, float t)
    {
        for(int i = 0; i < forces.Length; i++)
        {
            if(forces[i].IsEmpty())  //Claim the first empty force and then end the function
            {
                forces[i].AddForce(v, t);
                return;
            }
        }
    }

    public void Jump(float h)
    {
        if(cc.isGrounded)
        {
            JumpOverride(h);
        }
    }

    private float initialJumpTimer = 0f;
    public void JumpOverride(float h)
    {
        if (suspend)
            return;
        //cc.Move(Vector3.up * 0.5f);
        //transform.Translate(Vector3.up * 0.1f);
        gravity = -h;
        initialJumpTimer = Time.deltaTime * 2f;
    }

    [HideInInspector] public bool suspend = false;  //Set to true if you want to control the CC without CCM interfering

    public bool isUnderwater { private set; get; }

    // Update is called once per frame
    void Update()
    {
        bottomPos = cc.bounds.center + (Vector3.down * cc.bounds.size.y * 0.5f);
        topPos = cc.bounds.center + (Vector3.up * cc.bounds.size.y * 0.5f);


        if (suspend)
            return;
        bool lastUnderwater = isUnderwater;
        isUnderwater = topPos.y < WaterSurface.WaterY;
        

        if(lastUnderwater == false && isUnderwater)  //Just went underwater
        {
            gravity *= 0.1f;
        }

        if (moveVec == Vector2.zero)
        {
            forwardSpeed = Mathf.MoveTowards(forwardSpeed, 0f, accelerationPerSecond * Time.deltaTime * 3f);
            rightSpeed = Mathf.MoveTowards(rightSpeed, 0f, accelerationPerSecond * Time.deltaTime * 3f);
        }
        else
        {
            if(moveVec.x > 0f)
            {
                rightSpeed += accelerationPerSecond * moveVec.x * Time.deltaTime;
                if (rightSpeed > maxSpeed)
                    rightSpeed = maxSpeed;
            }
            if (moveVec.x < 0f)
            {
                rightSpeed += accelerationPerSecond * moveVec.x * Time.deltaTime;
                if (rightSpeed < -maxSpeed)
                    rightSpeed = -maxSpeed;
            }
            if (moveVec.y > 0f)
            {
                forwardSpeed += accelerationPerSecond * moveVec.y * Time.deltaTime;
                if (forwardSpeed > maxSpeed)
                    forwardSpeed = maxSpeed;
            }
            if (moveVec.y < 0f)
            {
                forwardSpeed += accelerationPerSecond * moveVec.y * Time.deltaTime;
                if (forwardSpeed < -maxSpeed)
                    forwardSpeed = -maxSpeed;
            }
        }

        //Gravity handling
        if (isUnderwater)
        {
            gravity -= waterVeritcal * 9.8f * Time.deltaTime;
        }
        else
        {
            //Not underwater- handle normally
            if (cc.isGrounded && initialJumpTimer <= 0f)
                gravity = 0f;
            else
                gravity += 9.8f * Time.deltaTime * gravityScale;


            if (initialJumpTimer > 0f)
                initialJumpTimer -= Time.deltaTime;


            if (gravity > TERMINAL_VELOCITY)
                gravity = TERMINAL_VELOCITY;
        }

        

        Vector3 mv = (Vector3.down * gravity);
        if (cc.isGrounded)
        {
            mv += (transform.forward * forwardSpeed) + (transform.right * rightSpeed);
        }
        else
            mv += (transform.forward * (forwardSpeed));


        //Process forces
        for(int i = 0; i < forces.Length; i++)
        {
            mv = mv + forces[i].CurrentVelocity();
        }

        if(Physics.Raycast(cc.bounds.center, mv, mv.magnitude, LayerMask.GetMask("Default")))
        {
            //You hit a wall- clear the forces.
            for(int i = 0; i < forces.Length; i++)
            {
                forces[i].Clear();
            }
        }

        cc.Move(mv * Time.deltaTime);
    }
}
