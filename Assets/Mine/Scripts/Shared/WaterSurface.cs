using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterSurface : MonoBehaviour
{

    public static float WaterY = -5000f;

    [Header("Only set to 0 if you don't want the water to bob.")]
    public float verticalRiseAndFall = 0f;
    public float verticalSpeedPerSecond = 0.05f;

    private float startingY = 0f;
    public GameObject splashPrefab;

    // Start is called before the first frame update
    void Start()
    {
        startingY = transform.position.y;
        GameObjectPool.InitPoolItem("Splash", splashPrefab, 50);
    }

    private void OnDestroy()
    {
        WaterY = -5000f;
    }

    bool isGoingUp = true;

    // Update is called once per frame
    void Update()
    {
        WaterY = transform.position.y;

        if(!Mathf.Approximately(verticalRiseAndFall, 0f))
        {
            float targetY = startingY - verticalRiseAndFall;
            if (isGoingUp)
                targetY = startingY + verticalRiseAndFall;

            Vector3 pos = transform.position;
            pos.y = Mathf.MoveTowards(pos.y, targetY, verticalSpeedPerSecond * Time.deltaTime);
            transform.position = pos;

            if (Mathf.Approximately(pos.y, targetY))
                isGoingUp = !isGoingUp;

        }

    }

    private void OnTriggerEnter(Collider other)
    {
        Vector3 p = other.transform.position;
        p.y = WaterY;
        GameObject splash = GameObjectPool.GetInstance("Splash", p, Quaternion.identity);
    }

}
