﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerModel : MonoBehaviour
{
    // Start is called before the first frame update
    private Vector3 startingScale;
    public CharacterControllerMovement ccInfo;

    Animator anim;
    void Start()
    {
        startingScale = transform.localScale;
        
    }

    public void SetScale(float s)
    {
        transform.localScale = startingScale * s;
    }


    // Update is called once per frame
    void Update()
    {
        if(anim != null)
            anim.feetPivotActive = 1f;

        Vector3 newPos = CrossVR.CurrentHMD.position;
        float angle = Mathf.Deg2Rad * (CrossVR.CurrentHMD.eulerAngles.y + 180f);
        newPos += new Vector3(Mathf.Sin(angle), 0f, Mathf.Cos(angle)) * 0.35f;

        transform.position = newPos;

        Vector3 rot = CrossVR.CurrentHMD.eulerAngles;
        rot.x = 0f;
        rot.z = 0f;
        transform.eulerAngles = rot;

        anim.SetBool("Is Grounded", ccInfo.cc.isGrounded);
        anim.SetBool("Is Walking", ccInfo.moveVec != Vector2.zero);

    }


    void OnAnimatorIK(int layerIndex)
    {
        if (anim == null)
            anim = GetComponent<Animator>();

        anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 1f);
        anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 1f);
        anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1f);
        anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1f);
        anim.SetIKPosition(AvatarIKGoal.RightHand, CrossVR.RightHand.position);
        anim.SetIKRotation(AvatarIKGoal.RightHand, CrossVR.RightHand.rotation);
        anim.SetIKPosition(AvatarIKGoal.LeftHand, CrossVR.LeftHand.position);
        anim.SetIKRotation(AvatarIKGoal.LeftHand, CrossVR.LeftHand.rotation);


        Quaternion q1 = Quaternion.Euler(new Vector3(CrossVR.CurrentHMD.eulerAngles.x, 0f, CrossVR.CurrentHMD.eulerAngles.z));
        anim.SetBoneLocalRotation(HumanBodyBones.Head, q1);
    }


}
