﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.XR;
#if UNITY_EDITOR || UNITY_STANDALONE
using Valve.VR;
#endif

public class PlayerMovement : MonoBehaviour
{
    public const float DEFAULT_HEIGHT = 1.8f, PERC_MOVE = 0.5f;
    public const float STROKE_LENGTH = 0.00225f;  //How many seconds should pass when you're swimming
    public const float STROKE_FORCE = 100f;  //Multiplier of how much force should be applied when you swim
    public Image calibrateImage;
    public Sprite mobileCalibrateSprite;
    public static Vector3 TeleportPos = Vector3.zero, TitlePos = Vector3.zero, PlayerPos = Vector3.zero;
    public static float Scale = 1f;
    public const float ROTATE_SPEED = 90f;
    private bool leftMenuDown = false, rightMenuDown = false;

    public CharacterControllerMovement ccm;
    public static float standingY = 0f;
    private float perc = 1f;
    
    public GameObject pauseMenu, calibrateMenu;
    private bool isCrouching = false;

    private bool firstUpdate = true;
    private bool isJumping = false;
    private bool initialScale = false;

    [Header("Leave blank if no PlayerModel gameobject.")]
    public Transform playerModel;


    private float swimTimer = 0f;  //Push the player based on hand momvenets
    

    // Start is called before the first frame update
    void Start()
    {
        SceneManager.activeSceneChanged += SceneManager_sceneLoaded;
        if (Application.isMobilePlatform)
            calibrateImage.sprite = mobileCalibrateSprite;
    }

    private void SceneManager_sceneLoaded(Scene arg0, Scene arg1)
    {
        firstUpdate = true;
        initialScale = false;
    }

    private void OnDestroy()
    {
        SceneManager.activeSceneChanged -= SceneManager_sceneLoaded;

    }



    // Update is called once per frame
    void Update()
    {
        if (firstUpdate && CrossVR.CurrentHMD != null)  //Reorient you back to 0,0,0 even if you're not standing there
        {

            if (CrossVR.CurrentHMD.localPosition != Vector3.zero)
            {
                

                //ccm.cc.Move(v);
                firstUpdate = false;

            }

        }
        if (initialScale == false)
        {
            if (CrossVR.CurrentRoot != null)
            {
                Vector3 scale = new Vector3(Scale, Scale, Scale);
                CrossVR.CurrentRoot.transform.localScale = scale;
                initialScale = true;

                Vector3 v = -CrossVR.CurrentHMD.localPosition;
                //Vector3 v = headPose.localPosition;
                v.y = 0f;
                transform.position = TeleportPos + v;

                if (playerModel != null)
                    playerModel.gameObject.SendMessage("SetScale", Scale);

            }
        }
       
 
        

        calibrateMenu.SetActive(Mathf.Approximately(standingY, 0f));
        
        if (!Mathf.Approximately(standingY, 0f))
        {

            //float dif = (standingY - head.localPosition.y);
            //perc = (dif) / DEFAULT_HEIGHT;
            perc = CrossVR.CurrentHMD.localPosition.y / standingY;
            //perc = 1f;  //TEMP
            float percTemp = Mathf.Clamp(perc, 0.2f, 1f);

            //Debug.Log("Standing Y: " + standingY + "Local pos" + head.localPosition.y + "Perc: " + perc);
            /*
            ccm.cc.height = DEFAULT_HEIGHT * percTemp;
            Vector3 center = ccm.cc.center;
            center.y = DEFAULT_HEIGHT * -0.5f * percTemp;
            ccm.cc.center = center;
            */

            //float percTemp = Mathf.Clamp(perc, 0.2f, 1f);
            ccm.cc.height = DEFAULT_HEIGHT * percTemp;
            //Vector3 center = ccm.cc.center;
            Vector3 c = transform.InverseTransformPoint(CrossVR.CurrentHMD.position);
            c.y = DEFAULT_HEIGHT * 0.5f * percTemp;

            ccm.cc.center = c;

            

            float h = ccm.cc.height * 0.6f;
            ccm.cc.stepOffset = h;

            Vector3 rot = CrossVR.CurrentHMD.transform.eulerAngles;
            rot.x = 0f;
            rot.z = 0f;
            transform.eulerAngles = rot;

            Vector3 hrPos = transform.position;
            CrossVR.CurrentRoot.position = hrPos;



            if (isJumping && ccm.cc.isGrounded)
                isJumping = false;



            float jumpH = standingY + (0.1016f * Scale);  //Standing Y + 4 inch.  Sometimes looking up activates this.
                if (CrossVR.CurrentHMD.localPosition.y >= jumpH)
                {
                    if (!isJumping)
                    {
                        isJumping = true;
                        ccm.JumpOverride(3.5f);
                    }

                }

           
        }
        PlayerPos = transform.position + ccm.cc.center;



        //Character controller editing
        //Vector3 c = head.localPosition;


        //Menu up replacement
        /*
        if (CrossVR.Left.menuDown)
            leftMenuDown = true;
        if (CrossVR.Right.menuDown)
            rightMenuDown = true;

        if (CrossVR.Left.menuUp)
            leftMenuDown = false;
        if (CrossVR.Right.menuUp)
            rightMenuDown = false;
        */
        leftMenuDown = CrossVR.Left.menuPressed;
        rightMenuDown = CrossVR.Right.menuPressed;


        if (CrossVR.Left.menuUp)
        {
            //Debug.Log("Head pose: " + CrossVR.CurrentHMD.localPosition + " vs character controller: " + ccm.cc.center + "Head root: " + CrossVR.CurrentRoot.position + " Vs player " + transform.position);
        }

        if (leftMenuDown && rightMenuDown)
        {
            if(Time.unscaledTime > (doubleMenuPressedTime + 2f))
            {
                doubleMenuPressedTime = Time.unscaledTime;
#if UNITY_EDITOR || UNITY_STANDALONE
                Valve.VR.OpenVR.Chaperone.ResetZeroPose(ETrackingUniverseOrigin.TrackingUniverseStanding);
#endif
                StartCoroutine(Calibrate());
            }


        }

        if (CrossVR.Right.menuUp)  //Only right menu pressed
        {
            if(Time.unscaledTime > (doubleMenuPressedTime + 2f))
            {
                pauseMenu.SetActive(!pauseMenu.activeSelf);
                if (pauseMenu.activeSelf == true)
                    Time.timeScale = 1f / 360f;
                else
                    Time.timeScale = 1f;
            }
            
        }


        //Menu down replacement
        if (CrossVR.Left.menuDown)
            leftMenuDown = true;
        if (CrossVR.Right.menuDown)
            rightMenuDown = true;

        

        //Joystick replacement
            //Move the character.

            Vector2 axis2 = CrossVR.Left.joystick;
            axis2.x = Mathf.Round(axis2.x * 2f) / 2f;
            axis2.y = Mathf.Round(axis2.y * 2f) / 2f;
            ccm.moveVec = axis2;
        if (ccm.isUnderwater)
        {
            ccm.moveVec = Vector2.zero;  //You're under the surface of water; use your arms to move.
            ccm.waterVeritcal = 1f;
            if (perc < 0.5f)  //Crouching- sink
                ccm.waterVeritcal = -1f;

            
            
        }


        swimTimer += Time.deltaTime;
        if (swimTimer > STROKE_LENGTH)
        {
            Vector3 f = Vector3.zero;
            float tempY = GripController.RightHandPos.y;
            if (tempY < WaterSurface.WaterY)
                f += CrossVR.Right.velocity * STROKE_FORCE;
            tempY = GripController.LeftHandPos.y;
            if (tempY < WaterSurface.WaterY)
                f += CrossVR.Left.velocity * STROKE_FORCE;


            Vector3 f2 = new Vector3(f.x * -2f, -f.y, -2f * f.z);

            if (f.y > 0f)  //Pushing you up- don't make it push you up as high
                f2.y *= 0.05f;

            Vector3 localStrokeVelocity = transform.InverseTransformDirection(f2);

            bool pushBackwards = false;
            if (localStrokeVelocity.z < 0f)
                pushBackwards = true;
            

            float timeMultiplier = 60f;  //Stroking forward- the force needs to last a while

            if(pushBackwards)
            {
                f2 = f2 * 0.1f;  //Smoother controls so you don't go forward in one stroke and back in the other.
                timeMultiplier = 2f;
            }

            ccm.AddForce(f2, STROKE_LENGTH * timeMultiplier);
            swimTimer = 0f;
        }

        Vector2 axis = CrossVR.Right.joystick;
        CrossVR.CurrentRoot.Rotate(new Vector3(0f, ROTATE_SPEED * axis.x * Time.deltaTime, 0f));

       

    }



  
    /*
    public void MenuUp(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        if (fromSource == SteamVR_Input_Sources.LeftHand)
            leftMenuDown = false;
        if (fromSource == SteamVR_Input_Sources.RightHand)
            rightMenuDown = false;


        
        if(fromSource == SteamVR_Input_Sources.LeftHand)
        {
            Debug.Log("Head pose: " + CrossVR.CurrentHMD.localPosition + " vs character controller: " + ccm.cc.center + "Head root: " + CrossVR.CurrentRoot.position + " Vs player " + transform.position);
        }
   
        if(fromSource == SteamVR_Input_Sources.RightHand && Time.unscaledTime + 2f > doubleMenuPressedTime)  //Only right menu pressed
        {
            pauseMenu.SetActive(!pauseMenu.activeSelf);
            if (pauseMenu.activeSelf == true)
                Time.timeScale = 1f / 360f;
            else
                Time.timeScale = 1f;
        }
    }
    */

    float doubleMenuPressedTime = 0f;
    /*
    public void MenuDown(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        if (fromSource == SteamVR_Input_Sources.LeftHand)
            leftMenuDown = true;
        if (fromSource == SteamVR_Input_Sources.RightHand)
            rightMenuDown = true;

        if (leftMenuDown && rightMenuDown)
        {
            //InputTracking.Recenter();
            StartCoroutine(Calibrate());
        }
            

    }
    */

    IEnumerator Calibrate()
    {

        Scale = DEFAULT_HEIGHT / (CrossVR.CurrentHMD.position.y - CrossVR.CurrentRoot.position.y);
        Vector3 scale = new Vector3(Scale, Scale, Scale);

        Vector3 lastScale = CrossVR.CurrentRoot.transform.localScale;
        CrossVR.CurrentRoot.transform.localScale = scale;

        if (playerModel != null)
            playerModel.gameObject.SendMessage("SetScale", Scale);

        while (CrossVR.CurrentRoot.transform.localScale == lastScale)
        {
            yield return new WaitForSecondsRealtime(Time.unscaledDeltaTime);
        }
		//Wait three frame steps to calibrate height
		for(int i = 0; i < 3; i++)
		{
			yield return new WaitForSecondsRealtime(Time.unscaledDeltaTime);
		}
        standingY = CrossVR.CurrentHMD.localPosition.y;
        transform.Translate(Vector3.up * 0.35f);

        yield return new WaitForSecondsRealtime(Time.unscaledDeltaTime);

        
        //ccm.cc.Move(Vector3.up * 0.15f);
        


        

        //Debug.Log("Standing Y: " + standingY);

        
        //Debug.LogError("Standing Y set to " + standingY);
    }

    
    /*
    public void Joystick(SteamVR_Action_Vector2 fromAction, SteamVR_Input_Sources fromSource, Vector2 axis, Vector2 delta)
    {
        
        if(fromSource == SteamVR_Input_Sources.LeftHand)
        {
            //Move the character.
            if (feetWalk == null)
                feetWalk = GetComponent<VR_walklocomotion>();

            if(feetWalk.disconnected)  //Foot sensors off- move via joystick
            {
                axis.x = Mathf.Round(axis.x * 2f) / 2f;
                axis.y = Mathf.Round(axis.y * 2f) / 2f;
                ccm.moveVec = axis;
            }
            
        }
        if(fromSource == SteamVR_Input_Sources.RightHand)
        {
            CrossVR.CurrentRoot.Rotate(new Vector3(0f, ROTATE_SPEED * axis.x * Time.deltaTime, 0f));
            
        }
    }
    */

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        hit.gameObject.SendMessage("ControlHit", new ControllerMessage(hit, gameObject), SendMessageOptions.DontRequireReceiver);
    }
}
