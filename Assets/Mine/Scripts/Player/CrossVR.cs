﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
#if UNITY_EDITOR || UNITY_STANDALONE
using Valve.VR;
#endif

public class CrossVR : MonoBehaviour
{
    [Space(20)]
    [Header("Steam VR Settings")]
    public GameObject steamVRRoot;
    public GameObject steamVRHead;
    public GameObject steamLeftHand;
    public GameObject steamRightHand;

#if UNITY_EDITOR || UNITY_STANDALONE
    public SteamVR_Action_Vector2 onJoysticksUpdate;
    public SteamVR_Action_Boolean onGripPressed, onTriggerPressed, onMenuPressed;
    public SteamVR_Action_Pose handPose;
#endif

    [Space(20)]
    [Header("Nolo VR Settings")]
    public GameObject questVRRoot;
    public GameObject questVRHead;
    public GameObject questLeftHand;
    public GameObject questRightHand;


    [Space(20)]
    [Header("Cross Settings")]
    public Transform crossLeftHand;
    public Transform crossRightHand;
    public Transform playerUI;

    public static Transform CurrentRoot = null, CurrentHMD = null, LeftHand = null, RightHand = null;


    public class Hand
    {
        public Hand()
        {

        }


        public bool triggerUp = false;
        public bool triggerDown = false;
        public bool triggerPressed = false;
        public bool gripUp = false;
        public bool gripDown = false;
        public bool gripPressed = false;
        public bool menuUp = false;
        public bool menuDown = false;
        public bool menuPressed = false;
        public Vector2 joystick;

        public Vector3 velocity;
    }

    public static Hand Left = new Hand();
    public static Hand Right = new Hand();

    // Start is called before the first frame update
    void Start()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            //Use nolo
            questVRRoot.SetActive(true);
            steamVRRoot.SetActive(false);


            crossLeftHand.parent = questLeftHand.transform;
            crossRightHand.parent = questRightHand.transform;

            CurrentRoot = questVRRoot.transform;
            CurrentHMD = questVRHead.transform;

        }
        else
        {
            //Use Steam VR
            questVRRoot.SetActive(false);
            steamVRRoot.SetActive(true);


            crossLeftHand.parent = steamLeftHand.transform;
            crossRightHand.parent = steamRightHand.transform;


            CurrentRoot = steamVRRoot.transform;
            CurrentHMD = steamVRHead.transform;
        }

        LeftHand = crossLeftHand;
        RightHand = crossRightHand;
        Vector3 v = playerUI.localPosition;
        playerUI.parent = CurrentHMD;
        playerUI.localPosition = v;
        playerUI.localEulerAngles = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            //Use Quest
            if (CurrentRoot == null)
                CurrentRoot = questVRRoot.transform;
            if (CurrentHMD == null)
                CurrentHMD = questVRHead.transform;

            Right.triggerUp = OVRInput.GetUp(OVRInput.Button.SecondaryIndexTrigger);
            Right.triggerDown = OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger);
            Right.triggerPressed = OVRInput.Get(OVRInput.Button.SecondaryIndexTrigger);
            Right.gripUp = OVRInput.GetUp(OVRInput.Button.SecondaryHandTrigger);
            Right.gripDown = OVRInput.GetDown(OVRInput.Button.SecondaryHandTrigger);
            Right.gripPressed = OVRInput.Get(OVRInput.Button.SecondaryIndexTrigger);
            Right.menuUp = OVRInput.GetUp(OVRInput.Button.Two);
            Right.menuDown = OVRInput.GetDown(OVRInput.Button.Two);
            Right.menuPressed = OVRInput.Get(OVRInput.Button.Two);
            Right.joystick = OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick);
            Right.velocity = OVRInput.GetLocalControllerVelocity(OVRInput.Controller.RHand);


            Left.triggerUp = OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger);
            Left.triggerDown = OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger);
            Left.triggerPressed = OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger);
            Left.gripUp = OVRInput.GetUp(OVRInput.Button.PrimaryHandTrigger);
            Left.gripDown = OVRInput.GetDown(OVRInput.Button.PrimaryHandTrigger);
            Left.gripPressed = OVRInput.Get(OVRInput.Button.PrimaryHandTrigger);
            Left.menuUp = OVRInput.GetUp(OVRInput.Button.Four);
            Left.menuDown = OVRInput.GetDown(OVRInput.Button.Four);
            Left.menuPressed = OVRInput.Get(OVRInput.Button.Four);
            Left.joystick = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick);
            Left.velocity = OVRInput.GetLocalControllerVelocity(OVRInput.Controller.LHand);
        }
        else
        {
            //Use Steam VR
            if (CurrentRoot == null)
                CurrentRoot = steamVRRoot.transform;
            if (CurrentHMD == null)
                CurrentHMD = steamVRHead.transform;
#if UNITY_EDITOR || UNITY_STANDALONE
            Left.gripDown = onGripPressed.GetStateDown(SteamVR_Input_Sources.LeftHand);
            Left.gripUp = onGripPressed.GetStateUp(SteamVR_Input_Sources.LeftHand);
            Left.gripPressed = onGripPressed.GetState(SteamVR_Input_Sources.LeftHand);
            Left.menuDown = onMenuPressed.GetStateDown(SteamVR_Input_Sources.LeftHand);
            Left.menuUp = onMenuPressed.GetStateUp(SteamVR_Input_Sources.LeftHand);
            Left.menuPressed = onMenuPressed.GetState(SteamVR_Input_Sources.LeftHand);
            Left.triggerDown = onTriggerPressed.GetStateDown(SteamVR_Input_Sources.LeftHand);
            Left.triggerUp = onTriggerPressed.GetStateUp(SteamVR_Input_Sources.LeftHand);
            Left.triggerPressed = onTriggerPressed.GetState(SteamVR_Input_Sources.LeftHand);
            Left.velocity = handPose.GetVelocity(SteamVR_Input_Sources.LeftHand);
            Left.joystick = onJoysticksUpdate.GetAxis(SteamVR_Input_Sources.LeftHand);


            Right.gripDown = onGripPressed.GetStateDown(SteamVR_Input_Sources.RightHand);
            Right.gripUp = onGripPressed.GetStateUp(SteamVR_Input_Sources.RightHand);
            Right.gripPressed = onGripPressed.GetState(SteamVR_Input_Sources.RightHand);
            Right.menuDown = onMenuPressed.GetStateDown(SteamVR_Input_Sources.RightHand);
            Right.menuUp = onMenuPressed.GetStateUp(SteamVR_Input_Sources.RightHand);
            Right.menuPressed = onMenuPressed.GetState(SteamVR_Input_Sources.RightHand);
            Right.triggerDown = onTriggerPressed.GetStateDown(SteamVR_Input_Sources.RightHand);
            Right.triggerUp = onTriggerPressed.GetStateUp(SteamVR_Input_Sources.RightHand);
            Right.triggerPressed = onTriggerPressed.GetState(SteamVR_Input_Sources.RightHand);
            Right.velocity = handPose.GetVelocity(SteamVR_Input_Sources.RightHand);
            Right.joystick = onJoysticksUpdate.GetAxis(SteamVR_Input_Sources.RightHand);
#endif
        }
    }
}
