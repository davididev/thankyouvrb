﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneVars : MonoBehaviour
{
    public GameObject boilerplatePrefab;
    public AudioClip musicInScene;
    public TextAsset dialogueOnStart;
    // Start is called before the first frame update
    void Start()
    {
        if (musicInScene != null)
            Invoke("PlaySong", Time.deltaTime);

        if (BoilerRoot.Count == 0)
        {
            GameObject g = GameObject.Instantiate(boilerplatePrefab, Vector3.zero, Quaternion.identity);
        }

        if(dialogueOnStart != null)
        {
            StartCoroutine(Dialogue());
        }
    }

    IEnumerator Dialogue()
    {
        while(Mathf.Approximately(PlayerMovement.standingY, 0))
        {
            yield return new WaitForSecondsNoTimeScale(0.1f);
        }
        yield return new WaitForSecondsNoTimeScale(0.1f);

        DialogueHandler.GetInstance().StartEvent(dialogueOnStart);
    }

    void PlaySong()
    {
        PlayMusic.PlaySong(musicInScene);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
