﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LadderRung : MonoBehaviour
{
    private const float MIN_DISTANCE = 0.001f;
    public static int GripCount = 0;
    private Vector3 startingLocalPosRight, startingLocalPosLeft;

    enum CurrentGrip { NONE, LEFT, RIGHT, BOTH};
    CurrentGrip currentlyGripping = CurrentGrip.NONE;

    public LayerMask blockingMask;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnDestroy()
    {
        GripCount = 0;
    }

    private CharacterControllerMovement playerCCM;

    // Update is called once per frame
    void Update()
    {
        if (playerCCM == null)
            playerCCM = GameObject.FindGameObjectWithTag("Player").GetComponent<CharacterControllerMovement>();

        bool leftIsHigher = true;
        if (GripController.RightHandPos.y > GripController.LeftHandPos.y)
            leftIsHigher = false;

        if (currentlyGripping == CurrentGrip.BOTH)
        {
            if(leftIsHigher)
            {
                Vector3 dif = transform.TransformPoint(startingLocalPosLeft) - GripController.LeftHandPos;
                ProcessDif(dif);
            }
            else
            {
                Vector3 dif = transform.TransformPoint(startingLocalPosRight) - GripController.RightHandPos;
                ProcessDif(dif);
            }
            
        }

        if (currentlyGripping == CurrentGrip.LEFT && (leftIsHigher || GripCount == 1))
        {
            Vector3 dif = transform.TransformPoint(startingLocalPosLeft) - GripController.LeftHandPos;
            ProcessDif(dif);
                
            
        }
        if (currentlyGripping == CurrentGrip.RIGHT && (!leftIsHigher || GripCount == 1))
        {
            Vector3 dif = transform.TransformPoint(startingLocalPosRight) - GripController.RightHandPos;
            ProcessDif(dif);

        }
    }

    void ProcessDif(Vector3 dif)
    {
        if (dif.sqrMagnitude > MIN_DISTANCE)
        {
            /*
            Vector3 origin = GripController.LeftHandPos;
            if(currentlyGripping == CurrentGrip.RIGHT)
                origin = GripController.RightHandPos;
            */
            Vector3 origin = playerCCM.cc.bounds.center;
            float scanDist = Vector3.Distance(Vector3.zero, dif) + playerCCM.cc.radius + 0.5f;

            //if(!Physics.Raycast(origin, dif.normalized, scanDist, blockingMask))  //Dont move CCM if wall is detected
            if (!Physics.BoxCast(playerCCM.cc.bounds.center, playerCCM.cc.bounds.extents, dif.normalized, playerCCM.transform.rotation, scanDist, blockingMask))  //Dont move CCM if wall is detected
            {
                if (playerCCM != null)
                    playerCCM.cc.Move(dif);
            }

                
            
        }
    }

    void OnGrip(bool isLeft)
    {
        if (playerCCM == null)
            playerCCM = GameObject.FindGameObjectWithTag("Player").GetComponent<CharacterControllerMovement>();

        GripCount++;
        if (isLeft)
        {
            if(currentlyGripping == CurrentGrip.RIGHT)  //Right was gripping this object earlier, change to both
                currentlyGripping = CurrentGrip.BOTH;
            else  //Right was NOT gripping this object earlier, just make it left.
                currentlyGripping = CurrentGrip.LEFT;
            startingLocalPosLeft = transform.InverseTransformPoint(GripController.LeftHandPos);
        }

        else
        {
            if (currentlyGripping == CurrentGrip.LEFT)  //Left was gripping this object earlier, change to both
                currentlyGripping = CurrentGrip.BOTH;
            else  //Left was NOT gripping this object earlier, just make it right.
                currentlyGripping = CurrentGrip.RIGHT;
            startingLocalPosRight = transform.InverseTransformPoint(GripController.RightHandPos);
        }

        playerCCM.suspend = true;
    }

    void OnRelease(bool isLeft)
    {
        if (playerCCM == null)
            playerCCM = GameObject.FindGameObjectWithTag("Player").GetComponent<CharacterControllerMovement>();
        GripCount--;
        if(currentlyGripping == CurrentGrip.BOTH)  //Was previously holding on with both hands- only release one
        {
            if (isLeft)
                currentlyGripping = CurrentGrip.RIGHT;
            else
                currentlyGripping = CurrentGrip.LEFT;
        }
        else  //Was previously holding on with one hand- release all.
            currentlyGripping = CurrentGrip.NONE;
        if (GripCount == 0)
            playerCCM.suspend = false;
    }
}
