﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//These make sure that we have the components that we need
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(ConfigurableJoint))]
[RequireComponent(typeof(FixedJoint))]
public class GripController : MonoBehaviour
{
    public const float DEFAULT_FORCE = 5f;
    //public SteamVR_Input_Sources Hand;//these allow us to set our input source and action.
    public bool HandLeft = true;
    //public SteamVR_Action_Boolean ToggleGripButton, TriggerButton;
    //public SteamVR_Action_Pose position;

    private GameObject ConnectedObject;//our current connected object
    public List<GameObject> NearObjects = new List<GameObject>();//all objects that we could pick up

    private Animator anim;

    public static bool uiMode = false;
    public LineRenderer lineRend;
    public SpriteRenderer crosshairRend;

    public static Vector3 LeftHandPos, RightHandPos;
    public static Quaternion LeftHandRot, RightHandRot;

    private void SceneManager_sceneLoaded(Scene arg0, Scene arg1)
    {
        NearObjects.Clear();
        ConnectedObject = null;
    }

    void Start()
    {
        SceneManager.activeSceneChanged += SceneManager_sceneLoaded;
    }


    void OnDestroy()
    {
        SceneManager.activeSceneChanged -= SceneManager_sceneLoaded;
    }

    private void Update()
    {
        if(HandLeft )
        {
            LeftHandPos = transform.position;
            LeftHandRot = transform.rotation;
        }
        if (!HandLeft)
        {
            RightHandPos = transform.position;
            RightHandRot = transform.rotation;
        }
        if (uiMode)
        {
            lineRend.gameObject.SetActive(true);
            crosshairRend.gameObject.SetActive(true);

            RaycastHit info;
            float maxDist = 100f;
            crosshairRend.transform.LookAt(CrossVR.CurrentHMD.position);
            if (Physics.Raycast(transform.position, transform.forward, out info, maxDist, LayerMask.GetMask("UI")))
            {
                lineRend.SetPositions(new Vector3[] { transform.position, info.point });
                crosshairRend.transform.position = info.point;

                if (info.collider.gameObject.tag == "UIButton")
                {
                    lineRend.startColor = Color.green;
                    lineRend.endColor = Color.green;
                    crosshairRend.color = Color.green;

                    info.collider.gameObject.SendMessage("OnHover", info.point, SendMessageOptions.DontRequireReceiver);
                    if(HandLeft && CrossVR.Left.triggerDown)
                    {
                        info.collider.gameObject.SendMessage("OnPress", SendMessageOptions.DontRequireReceiver);
                    }
                    if (!HandLeft && CrossVR.Right.triggerDown)
                    {
                        info.collider.gameObject.SendMessage("OnPress", SendMessageOptions.DontRequireReceiver);
                    }
                }
                else
                {
                    lineRend.startColor = Color.white;
                    lineRend.endColor = Color.white;
                    crosshairRend.color = Color.white;
                }
            }
            else
            {
                Vector3 pt = transform.position + (transform.forward * 20f);
                lineRend.SetPositions(new Vector3[] { transform.position, pt});
                crosshairRend.transform.position = pt;
            }
        }
        else  //Not UI mode
        {
            lineRend.gameObject.SetActive(false);
            crosshairRend.gameObject.SetActive(false);
        }


        if (anim == null)
            anim = GetComponent<Animator>();
        if (HandLeft && CrossVR.Left.gripUp)
            anim.SetBool("Grab", false);
        if (!HandLeft && CrossVR.Right.gripUp)
            anim.SetBool("Grab", false);

        if (ConnectedObject != null)//if we are holding somthing
        {
            if (ConnectedObject.GetComponent<Interactable>().touchCount == 0)//if our object isn't touching anything
            {
                //first, we disconnect our object
                GetComponent<ConfigurableJoint>().connectedBody = null;
                GetComponent<FixedJoint>().connectedBody = null;

                //now we step our object slightly towards the position of our controller, this is because the fixed joint just freezes the object in whatever position it currently is in relation to the controller so we need to move it to the position we want it to be in. We could just set position to the position of the controller and be done with it but I like the look of it swinging into position.
                //ConnectedObject.transform.position = Vector3.MoveTowards(ConnectedObject.transform.position, transform.position, .25f);
                //ConnectedObject.transform.rotation = Quaternion.RotateTowards(ConnectedObject.transform.rotation, transform.rotation, 10);

                //reconnect the body to the fixed joint
                GetComponent<FixedJoint>().connectedBody = ConnectedObject.GetComponent<Rigidbody>();
            }
            else if (ConnectedObject.GetComponent<Interactable>().touchCount > 0)//if it is touching something 
            {
                //switch from fixed joint to configurable joint
                GetComponent<FixedJoint>().connectedBody = null;
                GetComponent<ConfigurableJoint>().connectedBody = ConnectedObject.GetComponent<Rigidbody>();
            }

            if (HandLeft && CrossVR.Left.gripUp)// Check if we want to drop the object
            {    
                Release();
            }
            if (!HandLeft && CrossVR.Right.gripUp)// Check if we want to drop the object
            {
                Release();
            }
        }
        else//if we aren't holding something
        {
            if (HandLeft && CrossVR.Left.gripDown)//cheack if we want to pick somthing up
            {
                Grip();
            }
            if (!HandLeft && CrossVR.Right.gripDown)
            {
                Grip();
            }
        }


        if (HandLeft && CrossVR.Left.triggerDown && ConnectedObject != null)
        {
            ConnectedObject.SendMessage("OnTrigger", SendMessageOptions.DontRequireReceiver);
        }
        if (!HandLeft && CrossVR.Right.triggerDown && ConnectedObject != null)
        {
            ConnectedObject.SendMessage("OnTrigger", SendMessageOptions.DontRequireReceiver);
        }
        if (HandLeft && CrossVR.Left.triggerUp && ConnectedObject != null)
        {
            ConnectedObject.SendMessage("OnTriggerRelease", SendMessageOptions.DontRequireReceiver);
        }
        if (!HandLeft && CrossVR.Right.triggerUp && ConnectedObject != null)
        {
            ConnectedObject.SendMessage("OnTriggerRelease", SendMessageOptions.DontRequireReceiver);
        }

    }
      private void Grip()
     {
         GameObject NewObject = ClosestGrabbable();
         if (NewObject != null)
         {
             ConnectedObject = ClosestGrabbable();//find the Closest Grabbable and set it to the connected object
             ConnectedObject.GetComponent<Rigidbody>().useGravity = false;
             ConnectedObject.SendMessage("OnGrip", (HandLeft), SendMessageOptions.DontRequireReceiver);

            Interactable inte = ConnectedObject.GetComponent<Interactable>();
            if (inte.reorientOnGrip == true)
            {
                inte.Reorient(transform.forward);
            }
        }
        anim.SetBool("Grab", true);
    }
     private void Release()
     {
        if(ConnectedObject != null)
        {
            GetComponent<ConfigurableJoint>().connectedBody = null;
            GetComponent<FixedJoint>().connectedBody = null;
            ConnectedObject.GetComponent<Rigidbody>().useGravity = true;
            //ConnectedObject.GetComponent<Rigidbody>().velocity = position.GetVelocity(Hand) + transform.parent.GetComponent<Rigidbody>().velocity;
            //ConnectedObject.GetComponent<Rigidbody>().angularVelocity = position.GetAngularVelocity(Hand) + transform.parent.GetComponent<Rigidbody>().angularVelocity;
            if(HandLeft)
                ConnectedObject.GetComponent<Rigidbody>().AddForce(CrossVR.Left.velocity * DEFAULT_FORCE, ForceMode.Impulse);
            if(!HandLeft)
                ConnectedObject.GetComponent<Rigidbody>().AddForce(CrossVR.Right.velocity * DEFAULT_FORCE, ForceMode.Impulse);
            //ConnectedObject.GetComponent<Rigidbody>().AddTorque(position.angularVelocity * ANGULAR_FORCE, ForceMode.VelocityChange);
            ConnectedObject.SendMessage("OnRelease", (HandLeft), SendMessageOptions.DontRequireReceiver);
            ConnectedObject = null;
            
        }
         
        anim.SetBool("Grab", false);
    }
    void OnTriggerEnter(Collider other)
    {
        //Add grabbable objects in range of our hand to a list
        if (other.CompareTag("Grabbable"))
        {
            NearObjects.Add(other.gameObject);
        }
        Debug.Log(NearObjects);
    }
    void OnTriggerExit(Collider other)
    {
        //remove grabbable objects going out of range from our list
        if (other.CompareTag("Grabbable"))
        {
            NearObjects.Remove(other.gameObject);
        }
    }
    GameObject ClosestGrabbable()
    {
        //find the object in our list of grabbable that is closest and return it.
        GameObject ClosestGameObj = null;
        float Distance = float.MaxValue;
        if (NearObjects != null)
        {
            foreach (GameObject GameObj in NearObjects)
            {
                if ((GameObj.transform.position - transform.position).sqrMagnitude < Distance)
                {
                    ClosestGameObj = GameObj;
                    Distance = (GameObj.transform.position - transform.position).sqrMagnitude;
                }
            }
        }
        return ClosestGameObj;
    }
}