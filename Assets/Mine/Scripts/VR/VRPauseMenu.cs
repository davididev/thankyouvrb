﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VRPauseMenu : MonoBehaviour
{
    public GameObject mainPanel, confirmTitlePanel, confirmQuitPanel;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnEnable()
    {
        GripController.uiMode = true;
        GoToPanel(0);
    }

    private void OnDisable()
    {
        GripController.uiMode = false;
    }

    public void GoToPanel(int id)
    {
        mainPanel.SetActive(id == 0);
        confirmTitlePanel.SetActive(id == 1);
        confirmQuitPanel.SetActive(id == 2);
    }

    public void GoToTitle()
    {
        PlayerMovement.TeleportPos = PlayerMovement.TitlePos;
        SceneManager.LoadScene(0);
        Time.timeScale = 1f;
        gameObject.SetActive(false);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
