﻿using UnityEngine;
public class Interactable : MonoBehaviour
{
    public int touchCount;
    [SerializeField] private MeshRenderer rend;
    [SerializeField] private SkinnedMeshRenderer rendFallback;
    public bool reorientOnGrip = false;
    void start()
    {
        if (gameObject.tag != "Grabbable")
        {
            Debug.LogError("Interactable's tag is not set to grabbable");
        }
    }


    public void Reorient(Vector3 fwd)
    {
        //iTween.ValueTo(gameObject, iTween.Hash("from", transform.forward, "to", fwd, "time", 2f, "onupdate", "SetFWD"));
        transform.forward = fwd;
    }

    void SetFWD(Vector3 f)
    {
        transform.forward = f;
    }


    private void OnTriggerEnter(Collider collision)
    {
        if(collision.gameObject.layer == LayerMask.NameToLayer("Hand"))
        {
            touchCount++;
            SetGlowColor(new Color(0.2f, 0.2f, 0f, 1f));
            Vector3 v = collision.gameObject.transform.position;
            gameObject.SendMessage("OnHoverStart", v, SendMessageOptions.DontRequireReceiver);
        }
        
    }
    private void OnTriggerExit(Collider collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Hand"))
        {
            touchCount--;
            SetGlowColor(new Color(0.0f, 0.0f, 0f, 1f));
            Vector3 v = collision.gameObject.transform.position;
            gameObject.SendMessage("OnHoverEnd", v, SendMessageOptions.DontRequireReceiver);
        }

    }

    private void SetGlowColor(Color c)
    {
        if(rend != null)
            rend.material.SetColor("_EmissionColor", c);
        if (rendFallback != null)
            rendFallback.material.SetColor("_EmissionColor", c);
    }
}