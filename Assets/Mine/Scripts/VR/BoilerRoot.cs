using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoilerRoot : MonoBehaviour
{
    public static int Count = 0;
    // Start is called before the first frame update
    void Awake()
    {
        Object.DontDestroyOnLoad(transform);
        if (Application.isEditor)
            {
                //Load the save data here if needed.  You can remove "isEditor" if you don't add titles.
            }
        Count++;
    }

    private void OnDestroy()
    {
        Count--;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
