﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TheInternet : MonoBehaviour
{
    public AudioSource src;
    public AudioClip whatIsJenClip, wereGonnaDieClip;
    public GameObject explosionPrefab;


    private float initialGrabTime = -1f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnGrip(bool isLeft)
    {
        src.clip = whatIsJenClip;
        src.Play();

        initialGrabTime = Time.time;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (explosionPrefab.activeSelf == true)
            return;
        if(initialGrabTime > 0f && Time.time > (initialGrabTime + whatIsJenClip.length))
        {
            //GameObject.Instantiate(explosionPrefab, transform.position, Quaternion.identity);
            explosionPrefab.SetActive(true);
            src.clip = wereGonnaDieClip;
            src.Play();
            GetComponent<Animator>().SetBool("Dead", true);
        }
    }
}
