﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DancePad : MonoBehaviour
{
    private float lastTouchTime = 0f;
    public AudioSource sound;

    private Color currentEmission = Color.black, emissionTarget = Color.black;

    private MeshRenderer rend;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void ControlHit(ControllerMessage msg)
    {
        
        float dif = Mathf.Abs(Time.time - lastTouchTime);
        if(dif > 0.1f)
        {
            RaycastHit info;
            
            Vector3 origin = msg.info.point + Vector3.up;


            if (Physics.Raycast(origin, Vector3.down, out info, 10f, LayerMask.GetMask("Default")))
            {
                float myX = Mathf.Floor(info.textureCoord.x * 3f);
                float myY = Mathf.Floor(info.textureCoord.y * 3f);

                Debug.Log("Origin: " + info.textureCoord + " Py: " + info.point);

                float i = myY * 3f;
                i += myX;

                sound.pitch = 1f + ((i+1) / 9f);
                sound.Play();

            }
        }
        lastTouchTime = Time.time;
    }
    // Update is called once per frame
    void Update()
    {
        if (rend == null)
            rend = GetComponent<MeshRenderer>();
        rend.material.SetColor("_EmissionColor", currentEmission);

        if (currentEmission == emissionTarget)
        {
            int id = Random.Range(0, 5);
            if (id == 0)
                emissionTarget = new Color(0.2f, 0.1f, 0.1f);
            if (id == 1)
                emissionTarget = new Color(0.2f, 0.2f, 0.1f);
            if (id == 2)
                emissionTarget = new Color(0.2f, 0.1f, 0.1f);
            if (id == 3)
                emissionTarget = new Color(0.1f, 0.1f, 0.2f);
            if (id == 4)
                emissionTarget = new Color(0.2f, 0.1f, 0.2f);
            if (id == 5)
                emissionTarget = new Color(0.2f, 0.2f, 0.2f);
        }
        else
            currentEmission = Color.Lerp(currentEmission, emissionTarget, 0.5f);

    }
}
