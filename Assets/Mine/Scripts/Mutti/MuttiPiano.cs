﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuttiPiano : MonoBehaviour
{
    private AudioSource src;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (src == null)
            src = GetComponent<AudioSource>();
    }

    //Message called by Interactable
    void OnHoverStart(Vector3 handPos)
    {
        if (src == null)
            src = GetComponent<AudioSource>();

        Vector3 localPos = transform.InverseTransformPoint(handPos);
        float perc = 0.5f;
        perc = perc + localPos.x;
        perc = Mathf.Clamp(perc, 0f, 1f);

        
            

        src.pitch = perc + 0.5f;  //Between 0.5f and 1.5f
        src.Play();
    }
}
