using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatePerSecond : MonoBehaviour
{
    // Start is called before the first frame update
    public Vector3 rotationPerSecond;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(rotationPerSecond * Time.deltaTime);
    }
}
