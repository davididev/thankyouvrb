﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningGun : MonoBehaviour
{
    public LineRenderer lineRend;
    public AudioSource lightningSoundFX;

    public Transform triggerPoint;
    public const float MAX_DIST = 25f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTrigger()
    {
        if (lightningSoundFX != null)
            lightningSoundFX.Play();

        lineRend.gameObject.SetActive(true);
        Vector3 pt = triggerPoint.position + (triggerPoint.forward * MAX_DIST);
        RaycastHit info;
        if(Physics.Raycast(triggerPoint.position, triggerPoint.forward, out info, MAX_DIST, LayerMask.GetMask("Default", "Enemy")))
        {
            pt = info.point;
            info.collider.gameObject.SendMessage("OnShot", SendMessageOptions.DontRequireReceiver);
        }
        lineRend.SetPositions(new Vector3[] { triggerPoint.position, pt });
        float t = 0.1f;
        Invoke("TurnOffLineRend", t);
        iTween.ValueTo(gameObject, iTween.Hash("from", 1f, "to", 0f, "time", t, "onupdate", "LineSize"));
        
    }

    void LineSize(float f)
    {
        lineRend.widthMultiplier = f;
    }

    void TurnOffLineRend()
    {
        lineRend.gameObject.SetActive(false);
    }
}
