﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetShoot : MonoBehaviour
{
    public static int Count = 0;
    public GameObject explosionPrefab;
    // Start is called before the first frame update
    void Start()
    {
        GameObjectPool.InitPoolItem("TargetExplosion", explosionPrefab, 40);
    }

    private void OnEnable()
    {
        Count++;
    }

    private void OnDisable()
    {
        Count--;
    }

    void OnShot()
    {
        GameObject g = GameObjectPool.GetInstance("TargetExplosion", transform.position, Quaternion.identity);
        if (g != null)
            gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
