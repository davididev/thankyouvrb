﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetComputer : MonoBehaviour
{
    public TMPro.TextMeshProUGUI textObj;
    private int LastCount = 0;
    //public ScrollRect scrollRectangle;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    float value = -1f;
    void Update()
    {
        //textObj.text = "Targets left: " + TargetShoot.Count;

        Vector2 v = textObj.rectTransform.anchoredPosition;
        v.x -= Time.deltaTime * 50f;
        if (v.x < -200f)
        {
            LastCount = TargetShoot.Count;
            textObj.text = "Targets left: " + LastCount;
            v.x = 200f;
        }           
            

        textObj.rectTransform.anchoredPosition = v;
    }
}
