using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputerController : MonoBehaviour
{
    public Light[] lightsToColor;

    public MeshRenderer[] meshColor;

    // Start is called before the first frame update
    void Start()
    {
        foreach(MeshRenderer rend in meshColor)
        {
            rend.sharedMaterial.SetColor("_Color", Color.grey);
        }
    }

    void SetColor()
    {
        Color c = Color.red;

        float a1 = DialogueHandler.GetVar("%color");
        if (Mathf.Approximately(a1, 1f))
            c = Color.blue;
        if (Mathf.Approximately(a1, 2f))
            c = Color.green;


        for(int i = 0; i < lightsToColor.Length; i++)
        {
            lightsToColor[i].color = c;
        }
        foreach (MeshRenderer rend in meshColor)
        {
            rend.sharedMaterial.SetColor("_Color", c);
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
