﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteRod : MonoBehaviour
{
    // Start is called before the first frame update

    public AudioClip sound;
    public ParticleSystem ps;
    private AudioSource src;

    private Vector3 startingPos, startingRot;

    void Start()
    {
        src = gameObject.AddComponent<AudioSource>();
        src.minDistance = 5f;
        src.maxDistance = 25f;

        src.clip = sound;
        startingPos = transform.position;
        startingRot = transform.eulerAngles;

    }

    void OnTrigger()
    {
        float myX = transform.eulerAngles.x;
        src.pitch = 1f + ( (myX / 360f) * 1.5f);
        src.Play();
        ps.Play();
    }

    bool isReturning = false;

    void OnRelease(bool IsLeft)
    {
        if(isReturning == false)
        {
            Invoke("ReturnToNormal", 5f);
            isReturning = true;
        }
        
    }

    void ReturnToNormal()
    {
        isReturning = false;
        Rigidbody rigid = GetComponent<Rigidbody>();
        rigid.useGravity = false;
        rigid.velocity = Vector3.zero;
        rigid.angularVelocity = Vector3.zero;
        transform.position = startingPos;
        transform.eulerAngles= startingRot;
    }
}
