﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cloud : MonoBehaviour
{
    private float yOhY = 0f;  //The "y" of the top
    public bool follow = true;
    private Rigidbody rigid;
    private Vector3 startingPos;
    private AudioSource JumpSound;
    public Animator anim;
    private const float MOVE_SPEED = 80f, MIN_DISTANCE = 0.1f;
    public float JUMP_MULTIPLIER = 1f;
    float delayTimer = -2f;
    // Start is called before the first frame update
    void Start()
    {
        startingPos = transform.position;
        yOhY = GetComponent<BoxCollider>().bounds.max.y;
        //Debug.Log("Current Y: " + transform.position.y + " Top Y:" + yOhY);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (JumpSound == null)
            JumpSound = GetComponent<AudioSource>();
        if (rigid == null)
            rigid = GetComponent<Rigidbody>();
        
        if(follow)
        {
            Vector3 newPos = PlayerMovement.PlayerPos;
            newPos.y = startingPos.y;

            if (Vector3.Distance(newPos, rigid.position) > MIN_DISTANCE)
                rigid.position = newPos;

        }

        if (delayTimer >= 0f)
            delayTimer -= Time.fixedDeltaTime;

    }

    void ControlHit(ControllerMessage msg)
    {
        if(PlayerMovement.PlayerPos.y >= yOhY)
        if(msg.origin.tag == "Player")
        {
            if(delayTimer < 0f)
            {
                delayTimer = 0.2f;
                msg.origin.SendMessage("JumpOverride", (19f * JUMP_MULTIPLIER));
                JumpSound.Play();
                if (anim != null)
                    anim.SetTrigger("Bounce");
            }
            //msg.origin.GetComponent<CharacterControllerMovement>().JumpOverride(15f * JUMP_MULTIPLIER);
            
        }
    }
}
