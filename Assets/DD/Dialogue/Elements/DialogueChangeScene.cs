﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

using UnityEngine;

public class DialogueChangeScene : DialogueItem
{

	public float r = 0f, g = 0f, b = 0f;
	public const float FADE_TIME = 0.5f;  //How long it takes to fade in/out

	public string sceneToMoveTo = "SampleScene";
	public Vector3 positionToMoveTo = Vector3.zero;
	
    public DialogueChangeScene()
    {
        breakpoint = true;
    }
	
	public override IEnumerator Run()
    {
        yield return base.Run();

		PlayerMovement.TeleportPos = positionToMoveTo;
		AsyncOperation sync = SceneManager.LoadSceneAsync(sceneToMoveTo);
		sync.allowSceneActivation = false;
		Color c = new Color(r, g, b, 0f);

		while(c.a < 1f)
        {
			handle.flashImage.color = c;
			c.a += FADE_TIME * Time.unscaledDeltaTime;
			if (c.a > 1f)
				c.a = 1f;
			yield return new WaitForSecondsNoTimeScale(Time.unscaledDeltaTime);
		}
		handle.flashImage.color = c;

		sync.allowSceneActivation = true;
		while (!sync.isDone)
		{
			yield return new WaitForSecondsNoTimeScale(Time.unscaledDeltaTime);
		}

		while (c.a > 0f)
		{
			handle.flashImage.color = c;
			c.a -= FADE_TIME * Time.unscaledDeltaTime;
			if (c.a < 0f)
				c.a = 0f;
			yield return new WaitForSecondsNoTimeScale(Time.unscaledDeltaTime);
		}
		handle.flashImage.color = c;
		completed = true;
	}
	
	public override void DrawEditor(Rect verticalGroup)
    {
		GUILayout.Label("Scene to move to");
		sceneToMoveTo = GUILayout.TextField(sceneToMoveTo);
		
		
		GUILayout.BeginHorizontal();
		positionToMoveTo.x = this.FloatField("x", positionToMoveTo.x);
		positionToMoveTo.y = this.FloatField("y", positionToMoveTo.y);
		positionToMoveTo.z = this.FloatField("z", positionToMoveTo.z);
		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();
		GUILayout.Label("Color: ");
		r = this.FloatField("r", r);
		g = this.FloatField("g", g);
		b = this.FloatField("b", b);
		GUILayout.EndHorizontal();

	}
}
