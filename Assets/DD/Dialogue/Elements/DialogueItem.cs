﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

[XmlRoot("DialogueItem")]
[XmlInclude(typeof(DialogueString))]  //Need to include ALL subclasses
[XmlInclude(typeof(DialogueEnd))]
[XmlInclude(typeof(DialogueMoveActor))]
[XmlInclude(typeof(DialogueAnimation))]
[XmlInclude(typeof(DialogueGoTo))]
[XmlInclude(typeof(DialogueIfThen))]
[XmlInclude(typeof(DialogueSetVar))]
[XmlInclude(typeof(DialogueWait))]
[XmlInclude(typeof(DialogueChoice))]
[XmlInclude(typeof(DialogueSetActive))]
[XmlInclude(typeof(DialogueSendMessage))]
[XmlInclude(typeof(DialogueFlash))]
[XmlInclude(typeof(DialogueTeleportActor))]
[XmlInclude(typeof(DialogueChangeScene))]

public class DialogueItem
{


    [XmlIgnore] public bool breakpoint = false;
    [XmlIgnore] public bool completed { get; protected set; }  //Must be set manually in Run()
    [XmlIgnore] protected DialogueHandler handle;
    public virtual IEnumerator Run()
    {
        completed = false;  //In case the Run() happens twice
        handle = GameObject.FindWithTag("Dialogue Handler").GetComponent<DialogueHandler>();
        if(breakpoint)
        {
            while(RunBreakpoint() == false) { yield return null;  }
        }
        yield return null;
    }

    public virtual void DrawEditor(Rect verticalGroup)
    {

    }

    public bool RunBreakpoint()
    {
        return (handle.RUNNING_STACKS == 1);
    }
    

    public float FloatField(string label, float field)
    {
        string str = field.ToString();
        if (!str.Contains("."))
            str = str + ".0";
        GUILayout.Label(label);
        str = GUILayout.TextField(str);

        if (float.TryParse(str, out field))
            return field;
        else
            return 0f;
    }
    public int IntField(string label, int field)
    {
        string str = field.ToString();
        GUILayout.Label(label);
        str = GUILayout.TextField(str);

        if (int.TryParse(str, out field))
            return field;
        else
            return 0;
    }

    //Code from stack overflow
    public static System.Object GetValueOf(Type enumType, string enumConst)
    {
        if (enumType == null)
        {
            throw new ArgumentException("Specified enum type could not be found", "enumName");
        }

        object value = Enum.Parse(enumType, enumConst);
        return value;
    }

    public System.Enum EnumPopup(string label, System.Enum field)
    {
        string[] names = System.Enum.GetNames(field.GetType());
        Debug.Log("Field type: " + field.GetType().ToString());

        
        for(int i = 0; i < names.Length; i++)
        {
            if(i % 5 == 0)
                GUILayout.BeginHorizontal();
            bool current = false;
            int id = System.Convert.ToInt32(field);
            if (id == i)
                current = true;

            current = GUILayout.Toggle(current, names[i]);
            
            if (current == true)  //Was just checked
                field = (Enum) GetValueOf(field.GetType(), names[i]);
            

            if (i % 5 == 4)
                GUILayout.EndHorizontal();
        }
        GUILayout.EndHorizontal();
        return field;
    }
    
}
