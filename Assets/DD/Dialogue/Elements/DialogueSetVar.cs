﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class DialogueSetVar : DialogueItem
{
    public string varNameToSet = "%varname";
    public float varValue = 0f;
    public enum ALTER_TYPE { SET, ADD, SUBTRACT, MULTIPLY, DIVIDE, RANDOM, NIL };
    public ALTER_TYPE alterType = ALTER_TYPE.SET;

    public override IEnumerator Run()
    {
        yield return base.Run();
		breakpoint = true;
        if (alterType == ALTER_TYPE.SET)
        {
            DialogueHandler.SetVar(varNameToSet, varValue);
        }
        else
        {
            float f = DialogueHandler.GetVar(varNameToSet);
            if (alterType == ALTER_TYPE.ADD)
                f = f + varValue;
            if (alterType == ALTER_TYPE.SUBTRACT)
                f = f - varValue;
            if (alterType == ALTER_TYPE.MULTIPLY)
                f = f * varValue;
            if (alterType == ALTER_TYPE.DIVIDE)
                f = f / varValue;
			if (alterType == ALTER_TYPE.RANDOM)
				f = Mathf.Round(Random.Range(0f, varValue));

			DialogueHandler.SetVar(varNameToSet, f);
        }
        completed = true;
        yield return null;
    }

    public override void DrawEditor(Rect verticalGroup)
    {
        GUILayout.Label("Variable:");
        varNameToSet = GUILayout.TextField(varNameToSet);
        varValue = this.FloatField("Value", varValue);
        
		//alterType = (DialogueSetVar.ALTER_TYPE) this.EnumPopup("Alter by", alterType);
		
		
		
		GUILayout.Label("Alter Type: " +alterType.ToString());
		
		GUILayout.BeginHorizontal();
		if(alterType != ALTER_TYPE.SET)
		{
			if(GUILayout.Button("Set"))
				alterType = ALTER_TYPE.SET;
		}
		if(alterType != ALTER_TYPE.ADD)
		{
			if(GUILayout.Button("Add"))
				alterType = ALTER_TYPE.ADD;
		}
		if(alterType != ALTER_TYPE.SUBTRACT)
		{
			if(GUILayout.Button("Subtract"))
				alterType = ALTER_TYPE.SUBTRACT;
		}
		if(alterType != ALTER_TYPE.MULTIPLY)
		{
			if(GUILayout.Button("Multiply"))
				alterType = ALTER_TYPE.MULTIPLY;
		}
		if(alterType != ALTER_TYPE.DIVIDE)
		{
			if(GUILayout.Button("Divide"))
				alterType = ALTER_TYPE.DIVIDE;
		}
		if (alterType != ALTER_TYPE.RANDOM)
		{
			if (GUILayout.Button("Random"))
				alterType = ALTER_TYPE.RANDOM;
		}
		GUILayout.EndHorizontal();
		if (alterType == ALTER_TYPE.RANDOM)
			GUILayout.Label("Random will pick an integer between 0 and number set.");

	}

    public override string ToString()
    {
        return "Set variable";
    }
}
