﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System.Xml.Serialization;


public class DialogueEditorWindow : EditorWindow {
    

    public static List<DialogueItem> itemList = new List<DialogueItem>();
    public static DialogueItem currentItem;
    public static string fileName = "default";

    //ONLY CHANGE THIS BETWEEN GAMES
    public enum DIALOGUE_ITEMS { NONE, STRING, SET_VAR, IF_STATEMENT, MOVE_ACTOR, END_EVENT,
        ANIMATION, GOTO, WAIT, CHOICE, SET_ACTIVE, SEND_MSG, FLASH,
        TELEPORT_ACTOR, CHANGE_SCENE, };
    public DIALOGUE_ITEMS selected = DIALOGUE_ITEMS.NONE;

    public void DrawAddButtons()
    {
        selected = (DIALOGUE_ITEMS) EditorGUILayout.EnumPopup("Add a step", selected);
        if(selected != DIALOGUE_ITEMS.NONE)
        {
            if (selected == DIALOGUE_ITEMS.STRING)
            {
                itemList.Add(new DialogueString());
            }
            if (selected == DIALOGUE_ITEMS.SET_VAR)
            {
                itemList.Add(new DialogueSetVar());
            }
            if (selected == DIALOGUE_ITEMS.IF_STATEMENT)
            {
                itemList.Add(new DialogueIfThen());
            }
            
            if (selected == DIALOGUE_ITEMS.MOVE_ACTOR)
            {
                itemList.Add(new DialogueMoveActor());
            }
            if (selected == DIALOGUE_ITEMS.END_EVENT)
            {
                itemList.Add(new DialogueEnd());
            }
            if (selected == DIALOGUE_ITEMS.ANIMATION)
            {
                itemList.Add(new DialogueAnimation());
            }
            if (selected == DIALOGUE_ITEMS.GOTO)
            {
                itemList.Add(new DialogueGoTo());
            }
            if (selected == DIALOGUE_ITEMS.WAIT)
            {
                itemList.Add(new DialogueWait());
            }
            if (selected == DIALOGUE_ITEMS.CHOICE)
            {
                itemList.Add(new DialogueChoice());
            }
            if (selected == DIALOGUE_ITEMS.SET_ACTIVE)
            {
                itemList.Add(new DialogueSetActive());
            }
            if (selected == DIALOGUE_ITEMS.SEND_MSG)
            {
                itemList.Add(new DialogueSendMessage());
            }
            if (selected == DIALOGUE_ITEMS.FLASH)
            {
                itemList.Add(new DialogueFlash());
            }
            if (selected == DIALOGUE_ITEMS.TELEPORT_ACTOR)
            {
                itemList.Add(new DialogueTeleportActor());
            }
			if (selected == DIALOGUE_ITEMS.CHANGE_SCENE)
            {
                itemList.Add(new DialogueChangeScene());
            }

            selected = DIALOGUE_ITEMS.NONE;
        }
    }
    //END CHANGE THIS BETWEEN GAMES
    

    [MenuItem("Window/DialogueEditing")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(DialogueEditorWindow));
    }

    Rect insideButton;
    void OnGUI()
    {
        insideButton = EditorGUILayout.BeginHorizontal();
        Header();

        EditorGUILayout.EndHorizontal();

        for(int i = 0; i < 15; i++)  //Space for the scroll bar
        {
            EditorGUILayout.Space();
        }
        
        
        insideButton = EditorGUILayout.BeginHorizontal();

        

        scrollPos = EditorGUILayout.BeginScrollView(scrollPos, true, false);
        DrawEntries();
        EditorGUILayout.EndScrollView();
        DrawData();
        
        EditorGUILayout.EndHorizontal();
        

    }

    
    void Header()
    {
        EditorGUILayout.LabelField("File Name");
        fileName = EditorGUILayout.TextField(fileName);

        if (fileName == "default")
        {
            if (File.Exists("lastfile.dat"))
            {
                fileName = File.ReadAllText("lastfile.dat");
            }
        }


        if (GUILayout.Button("Save"))
            Save();
        if (GUILayout.Button("Load"))
            Load();

        DrawAddButtons();


    }

    void Save()
    {
        File.WriteAllText("lastfile.dat", fileName);
        Debug.Log("<color=green>Saved to root of assets: " + fileName + ".xml</color>");
        DialogueItem[] array = itemList.ToArray();
        DialogueHolder hold = new DialogueHolder();
        hold.items = array;
        


        var serializer = new XmlSerializer(typeof(DialogueHolder));
        var stream = new FileStream(Path.Combine(Application.dataPath, fileName + ".xml"), FileMode.Create);
        serializer.Serialize(stream, hold);
        stream.Close();
		
		AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
    }

    void Load()
    {
        try
        {
            var serializer = new XmlSerializer(typeof(DialogueHolder));
            DialogueHolder dh = serializer.Deserialize(new StringReader(File.ReadAllText(Path.Combine(Application.dataPath, fileName + ".xml")))) as DialogueHolder;
            
            
            itemList.Clear();
            foreach (DialogueItem i in dh.items)
            {
                itemList.Add(i);
            }
            Debug.Log("<color=green>Loaded from root of assets: " + fileName + ".xml</color>");
        }
        catch(System.Exception e)
        {
            Debug.Log("<color=red>Could not load " + fileName + ".xml from root of assets.  </color>" + e.ToString());
        }
    }

   

    void RecreateListFromArray(DialogueItem[] myList)
    {
        itemList = new List<DialogueItem>();
        for(int i = 0; i < myList.Length; i++)
        {
            itemList.Add(myList[i]);
        }
    }

    //Draw the original list of items
    void DrawEntries()
    {
        Rect verticalGroup = EditorGUILayout.BeginVertical();
        //Entries
        List<DialogueItem>.Enumerator e1 = itemList.GetEnumerator();
        int id = 0;
        while (e1.MoveNext())
        {
            GUILayout.BeginHorizontal();

            //Move down
            if (id == (itemList.Count - 1))
            {
                GUILayout.Button(" ", GUILayout.Width(30f));
            }
            else
            {
                if (GUILayout.Button("▼", GUILayout.Width(30f)))
                {
                    //Move items up
                    DialogueItem[] tempList = itemList.ToArray();
                    DialogueItem copy1 = tempList[id];
                    DialogueItem copy2 = tempList[id + 1];

                    tempList[id] = copy2;
                    tempList[id + 1] = copy1;
                    RecreateListFromArray(tempList);
                    return;
                }
            }
            if (GUILayout.Button(id + ":" + e1.Current.ToString()))
            {
                
                currentItem = null;
                currentItem = e1.Current;
            }

            //Move up
            if (id == 0)
            {
                GUILayout.Button(" ", GUILayout.Width(30f));
            }
            else
            {
                if (GUILayout.Button("▲", GUILayout.Width(30f)))
                {
                    //Move items up

                    DialogueItem[] tempList = itemList.ToArray();
                    DialogueItem copy1 = tempList[id];
                    DialogueItem copy2 = tempList[id - 1];

                    tempList[id] = copy2;
                    tempList[id - 1] = copy1;
                    RecreateListFromArray(tempList);
                    return;
                }
            }

            if (GUILayout.Button("X", GUILayout.Width(30f)))
            {
                currentItem = null;
                itemList.Remove(e1.Current);
                break;
            }
            GUILayout.EndHorizontal();
            id++;
        }
        EditorGUILayout.EndVertical();
    }

    Vector2 scrollPos;
    //Draw the selected item in data
    void DrawData()
    {
        Rect verticalGroup = EditorGUILayout.BeginVertical();
        if (currentItem != null)
        {
            //Close current item
            if (GUI.Button(new Rect(verticalGroup.min.x, (125f), verticalGroup.width * 0.75f, 30f), "Close"))
            {
                currentItem = null;
                return;
            }
            GUILayout.Space(150f);
            GUILayout.Label("---Edit the item you just clicked---");
            currentItem.DrawEditor(verticalGroup);
        }
        EditorGUILayout.EndVertical();
    }

    
}
